<?php

/**
 * Config for iPipeline connection details.
 */

return [
    'username' => '',
    'password' => '',
    'appname'  => '',
    'token'    => '',
    'dns'      => 'https://reapext.assureweb.co.uk/APe2',
];