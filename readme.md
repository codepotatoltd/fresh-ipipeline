# Example Laravel project for connecting to iPipeline's APE life insurance API. 

## Heads up, you'll struggle unless:
- You are comfortable using PHP7
- You've used and understand the Laravel framework 
- You're familiar using Composer
- You've integrated with SOAP services using PHP's SoapClient

## Setup Process
1. Composer install
2. Setup Laravel as per normal
3. Add API credentials to ipipeline.php config file
4. Hookup QuoteController.php methods to your own routes and adjust as required

-- 
If you have any questions on the code used here please feel free to contact me. 

Created by gareth@codepotato.co.uk.