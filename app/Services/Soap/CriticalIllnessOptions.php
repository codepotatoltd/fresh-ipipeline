<?php

namespace App\Services\Soap;

class CriticalIllnessOptions
{

    public $CriticalIllnessBuyBack;
    public $CriticalIllnessCoverAmount;
    public $LifeOrEarlierCriticalIllnessCoverAmount;

    public function __construct($criticalIllnessBuyBack, $criticalIllnessCoverAmount, $lifeOrEarlierCriticalIllnessCoverAmount)
    {
        $this->CriticalIllnessBuyBack = $criticalIllnessBuyBack;
        $this->CriticalIllnessCoverAmount = $criticalIllnessCoverAmount;
        $this->LifeOrEarlierCriticalIllnessCoverAmount = $lifeOrEarlierCriticalIllnessCoverAmount;
    }

}