<?php

namespace App\Services\Soap;

class Benefit
{

    public $BenefitAmount;
    public $CriticalIllness;
    public $CriticalIllnessOptions;
    public $ExcludeLowStart;
    public $FractureCover = 'None';
    public $Indexation = 'Level';
    public $Life;
    public $LifeCoverBuyBack;
    public $LifeOrEarlierCriticalIllness;
    public $LivesAssuredBasis;
    public $PremiumAmount;
    public $PremiumFrequency;
    public $PremiumType;
    public $Renewable;
    public $TermYears;
    public $ToAge;
    public $TotalPermanentDisability;
    public $WaiverOfPremium;
    public $type = 'RequestTermBenefit';

    public function __construct(
        $LivesAssuredBasis = 'FirstLife',
        $BenefitAmount = 55000,
        $CriticalIllness,
        $CriticalIllnessBuyBack,
        $CriticalIllnessCoverAmount,
        $LifeOrEarlierCriticalIllnessCoverAmount,
        $ExcludeLowStart = false,
        $FractureCover,
        $Indexation,
        $Life = true,
        $LifeCoverBuyBack = false,
        $LifeOrEarlierCriticalIllness = false,
        $PremiumAmount = null,
        $PremiumFrequency = 'Monthly',
        $PremiumType = 'Guaranteed',
        $Renewable = false,
        $TermYears = 11,
        $ToAge = null,
        $TotalPermanentDisability = 'None',
        $WaiverOfPremium = 'None')
    {
        $this->LivesAssuredBasis = $LivesAssuredBasis;
        $this->BenefitAmount = $BenefitAmount;
        $this->CriticalIllness = $CriticalIllness;
        if ($this->CriticalIllness) {
            $this->CriticalIllnessOptions = new CriticalIllnessOptions($CriticalIllnessBuyBack, $CriticalIllnessCoverAmount, $LifeOrEarlierCriticalIllnessCoverAmount);
        }
        $this->ExcludeLowStart = $ExcludeLowStart;
        $this->FractureCover = $FractureCover;
        $this->Indexation = $Indexation;
        $this->Life = $Life;
        $this->LifeCoverBuyBack = $LifeCoverBuyBack;
        $this->LifeOrEarlierCriticalIllness = $LifeOrEarlierCriticalIllness;
        if ($this->BenefitAmount > 0) {
            $this->PremiumAmount = null;
        } else {
            $this->PremiumAmount = $PremiumAmount;
        }
        $this->PremiumFrequency = $PremiumFrequency;
        $this->PremiumType = $PremiumType;
        $this->Renewable = $Renewable;
        $this->TermYears = $TermYears;
        $this->ToAge = $ToAge;
        $this->TotalPermanentDisability = $TotalPermanentDisability;
        $this->WaiverOfPremium = $WaiverOfPremium;
        $this->type = 'RequestTermBenefit';
    }


}