<?php

/**
 * LifeQuote class built for interacting with the iPipeline APE API.
 */

namespace App\Services;

use App\Exceptions\IpipelineError;
use App\Http\Requests\SubmitQuoteData;
use App\Services\Soap\Benefit;
use App\Services\Soap\CriticalIllness;
use App\Services\Soap\CriticalIllnessOptions;
use App\Services\Soap\RequestTermBenefit;
use App\Services\WsseAuthHeader;
use Carbon\Carbon;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class LifeQuote
{

    protected $who;
    protected $term;
    protected $include_critical_illness;
    protected $increasing_benefit;
    protected $quote_based_on;
    protected $cover_amount;
    protected $benefit_amount;
    protected $client_one_gender;
    protected $client_one_dob;
    protected $client_one_smoker;
    protected $client_one_title;
    protected $client_one_first_name;
    protected $client_one_last_name;
    protected $client_one_email;
    protected $client_two_gender;
    protected $client_two_dob;
    protected $client_two_smoker;
    protected $client_two_title;
    protected $client_two_first_name;
    protected $client_two_last_name;
    protected $client_two_email;
    protected $livesAssured = 'FirstLife';
    protected $client;
    protected $quotation_basis = 'SumAssured';

    /**
     * @param $action
     */
    public function buildClient($action)
    {

        if ($action === 'SubmitComparisonQuoteRequest') {
            $this->client = new \SoapClient(config('ipipeline.dns') . '/enhancedprotectioncomparisonservice.svc?wsdl', [
                'trace' => true,
                'wsdl_cache' => false,
                'soap_version' => SOAP_1_2,
                'classmap' => [
                    'RequestTermBenefit' => Benefit::class,
                    'RequestCriticalIllnessOptions' => CriticalIllnessOptions::class
                ]
            ]);
        } else {
            $this->client = new \SoapClient(config('ipipeline.dns') . '/enhancedprotectioncomparisonservice.svc?wsdl', [
                'trace' => true,
                'wsdl_cache' => false,
                'soap_version' => SOAP_1_2
            ]);
        }

        $this->client->__setSoapHeaders($this->GetSoapHeader($action));

        return $this->client;
    }

    private function GetSoapActionHeader($soapaction)
    {
        $addressingNamespace = 'http://www.w3.org/2005/08/addressing';
        $actionHeader = new \SoapHeader($addressingNamespace, 'Action', $soapaction);

        return $actionHeader;
    }


    private function GetSoapHeader($soapaction)
    {
        return [
            $this->GetSoapActionHeader($soapaction),
            new WsseAuthHeader(config('ipipeline.username'), config('ipipeline.password')),
        ];
    }

    private function setClientOne($gender, $dob, $smoker, $title, $first_name, $last_name, $email)
    {
        $this->client_one_gender = $gender;
        $this->client_one_dob = $this->formatDob($dob);
        $this->client_one_smoker = $smoker;
        $this->client_one_title = $title;
        $this->client_one_first_name = $first_name;
        $this->client_one_last_name = $last_name;
        $this->client_one_email = $email;
    }

    private function setLivesAssured($selection)
    {
        $this->who = $selection;
        if ($this->who === 'joint') {
            $this->livesAssured = 'JointLifeFirstDeath';
        }
    }

    private function setClientTwo($gender, $dob, $smoker, $title, $first_name, $last_name, $email)
    {
        $this->client_two_gender = ucfirst($gender);
        $this->client_two_dob = $this->formatDob($dob);
        $this->client_two_smoker = $smoker;
        $this->client_two_title = $title;
        $this->client_two_first_name = $first_name;
        $this->client_two_last_name = $last_name;
        $this->client_two_email = $email;
    }

    private function getBenefitAmount($request)
    {
        if ($request->get('quote_based_on') === 'benefit') {
            return $request->get('benefit_amount');
        }

        return null;
    }

    private function getPremiumAmount($request)
    {
        if ($request->get('quote_based_on') === 'premium') {
            $this->quotation_basis = 'Premium';
            return $request->get('premium_amount');
        }

        return null;
    }

    private function formatDob($oldDob)
    {
        return Carbon::createFromFormat('d/m/Y', $oldDob)->format('Y-m-d');
    }

    public function getComparisonData(SubmitQuoteData $request)
    {

        $this->setClientOne($request->get('client_one_gender'), $request->get('client_one_dob'), $request->get('client_one_smoker'), $request->get('client_one_title'), $request->get('client_one_first_name'), $request->get('client_one_last_name'), $request->get('client_one_email'));

        if ($request->get('who') === 'joint') {
            $this->setLivesAssured($request->get('who'));
            $this->setClientTwo($request->get('client_two_gender'), $request->get('client_two_dob'), $request->get('client_two_smoker'), $request->get('client_two_title'), $request->get('client_two_first_name'), $request->get('client_two_last_name'), $request->get('client_two_email'));
        }

        $providersRequested = null;
        $illustrationBasis = "Quote";
        $businessType = "DesignatedInvestment";
        $quoteReferences = array(
            "AdviceProcessId" => "",
            "ExternalReferenceId" => "",
            "SessionId" => "",
            "TransactionId" => ''
        );

        $transactionHeaders = array(
            "BusinessType" => $businessType,
            "IllustrationBasis" => $illustrationBasis,
            "QuoteReferences" => $quoteReferences
        );

        $firstLife = array(
            "Title" => $this->client_one_title,
            "Forename" => $this->client_one_first_name,
            "Surname" => $this->client_one_last_name,
            "DateOfBirth" => $this->client_one_dob,
            "Gender" => $this->client_one_gender,
            'Occupation' => 'AAB00011',
            "Smoker" => $this->client_one_smoker,
            "EmploymentStatus" => 'Employed',
            'Salary' => 30000
        );

        $secondLife = array(
            "Title" => $this->client_two_title,
            "Forename" => $this->client_two_first_name,
            "Surname" => $this->client_two_last_name,
            "DateOfBirth" => $this->client_two_dob,
            "Gender" => $this->client_two_gender,
            'Occupation' => 'AAB00011',
            "Smoker" => $this->client_two_smoker,
            "EmploymentStatus" => 'Employed',
            'Salary' => 30000
        );
        $livesAssured = array(
            "FirstLife" => $firstLife,
        );

        if ($this->livesAssured === 'JointLifeFirstDeath') {
            $livesAssured['SecondLife'] = $secondLife;
        }

        $policyBasis = [
            'Benefits' => [
                'RequestBenefit' => [
                    'Benefit' => new Benefit(
                        $this->livesAssured,
                        $this->getBenefitAmount($request),
                        $request->get('include_critical_illness'),
                        null,
                        $this->getBenefitAmount($request),
                        null,
                        false,
                        'None',
                        'Level',
                        true,
                        false,
                        false,
                        $this->getPremiumAmount($request),
                        'Monthly',
                        'Guaranteed',
                        false,
                        $request->get('years'),
                        null,
                        'None',
                        'None'
                    ),
                    'BenefitType' => 'Term'
                ],
            ],
            "LivesAssured" => $livesAssured,
            'NumberOfBenefitsChosen' => 1,
            'QuotationBasis' => $this->quotation_basis
        ];

        $commissionOptions = array(
            "IndemnityRequired" => true,
            "CommissionType" => "Full",
            "CommissionPercentage" => null,
        );


        $ComparisonQuoteRequest = array(
            "TransactionHeaders" => $transactionHeaders,
            "PolicyBasis" => $policyBasis,
            "CommissionOptions" => $commissionOptions,
            "ProvidersRequested" => $providersRequested
        );


        $appname = array("Application" => config('ipipeline.appname'));
        $req = array("Header" => $appname, "ComparisonQuoteRequest" => $ComparisonQuoteRequest);

        return ['request' => $req];
    }

    public function sendComparisonRequest(SubmitQuoteData $request)
    {

        $response = [];

        try {
            $this->buildClient('SubmitComparisonQuoteRequest');
            $xml = $this->client->SubmitComparisonQuoteRequest($this->getComparisonData($request));

            $response = [
                'type' => 'success',
                'quote_uuid' => $xml->SubmitComparisonQuoteRequestResult->SubmitComparisonQuoteResponse->TransactionHeaders->QuoteReferences->TransactionId
            ];

        } catch (\SoapFault $e) {
            Log::error("SOAP FAULT - Request: \n " . ($this->client->__getLastRequest()) . " \n ");
            Log::error("SOAP FAULT - Response: \n " . ($this->client->__getLastResponseHeaders()) . " \n ");
            Log::error("SOAP FAULT - Response: \n " . ($this->client->__getLastResponse()) . " \n ");
            Log::error("SOAP FAULT - MESSAGE: - " . $e->getMessage());
            $response = [
                'type' => 'failure',
                'message' => 'Unfortunately something went wrong. Please check your inputs and try again.'
            ];
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $response = [
                'type' => 'failure',
                'message' => 'Unfortunately something went wrong. Please check your inputs and try again.'
            ];
        }

        return response()->json($response, ($response['type'] === 'failure' ? 500 : 200));

    }

    protected function getRequestResultsData(Quote $quote)
    {

        $appname = array("Application" => config('ipipeline.appname'));
        $req = [
            "Header" => $appname,
            "ComparisonQuoteResultsRequest" => [
                "ComparisonId" => $quote->getRouteKey()
            ]
        ];

        return ['request' => $req];
    }

    public function getResults(Quote $quote)
    {

        if ($quote->status === 'Pending') {

            $response = [];

            try {
                $this->buildClient('RetrieveComparisonQuoteResults');
                $xml = $this->client->RetrieveComparisonQuoteResults($this->getRequestResultsData($quote));
                $response['type'] = 'success';

            } catch (\SoapFault $e) {
                Log::error("SOAP FAULT - Request: \n " . ($this->client->__getLastRequest()) . " \n ");
                Log::error("SOAP FAULT - Response: \n " . ($this->client->__getLastResponseHeaders()) . " \n ");
                Log::error("SOAP FAULT - Response: \n " . ($this->client->__getLastResponse()) . " \n ");
                Log::error("SOAP FAULT - MESSAGE: - " . $e->getMessage());
                $response = [
                    'type' => 'failure',
                    'message' => 'Unfortunately something went wrong. Please check your inputs and try again.'
                ];
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                $response = [
                    'type' => 'failure',
                    'message' => 'Unfortunately something went wrong. Please check your inputs and try again.'
                ];
            }

            if ($response['type'] === 'success') {
                return $this->processResults($quote, $xml);
            }
        } elseif ($quote->status === 'Completed') {
            $results = $quote->results()->orderBy('total_premium', 'asc')->get();

            return response()->json([
                'type' => 'success',
                'results' => $results,
                'quote' => $quote
            ]);
        }

    }

    private function processResults(Quote $quote, $xml)
    {

        if ($quote->status === 'Pending') {

            if (isset($xml->RetrieveComparisonQuoteResultsResult->EnhancedProtectionComparisonQuoteResult->EnhancedProtectionQuoteResults->EnhancedProtectionQuoteResult)) {


                if (count($xml->RetrieveComparisonQuoteResultsResult->EnhancedProtectionComparisonQuoteResult->EnhancedProtectionQuoteResults->EnhancedProtectionQuoteResult) > 0) {
                    foreach ($xml->RetrieveComparisonQuoteResultsResult->EnhancedProtectionComparisonQuoteResult->EnhancedProtectionQuoteResults->EnhancedProtectionQuoteResult as $result) {

                        if ($result->ProviderQuoteStatus->Status !== 'AwaitingResponse'
                            && $result->ProviderQuoteStatus->Status !== 'BusinessError'
                            && $result->ProviderQuoteStatus->Status !== 'ProviderUnavailable'
                            && $result->TotalPremium > '0'
                            && $result->ProviderQuoteStatus->Status !== 'InternalError') {

                            try {

                                // process data here, build $results

                            } catch (\Exception $e) {
                                Log::debug($result);
                            }
                        }

                    }
                }


                if ($quote->status === 'Completed') {

                    return response()->json([
                        'type' => 'success',
                        'results' => $results,
                        'quote' => $quote
                    ]);

                }

                return response()->json([
                    'type' => 'retry',
                    'results' => $results,
                    'quote' => $quote
                ]);

            } else {
                return response()->json(['type' => 'retry', 'results' => []]);
            }
        } elseif ($quote->status === 'Completed') {
            $results = []

            return response()->json([
                'type' => 'success',
                'results' => $results,
                'quote' => $quote
            ]);
        }

    }

}
