<?php
/**
 * Created by Gareth@codepotato.co.uk
 * Date: 26/01/2018
 * Time: 15:29
 *
 */

namespace App\Http\Controllers;
use App\Http\Requests\SubmitQuoteData;
use App\Services\LifeQuote;

class QuoteController
{
    protected $lifequote;

    public function __construct() {
        $this->lifequote = new LifeQuote();
    }

    public function submit( SubmitQuoteData $request ) {
        return $this->lifequote->sendComparisonRequest( $request );
    }

    public function getResults( Quote $quote ) {
        return $this->lifequote->getResults( $quote );
    }
}