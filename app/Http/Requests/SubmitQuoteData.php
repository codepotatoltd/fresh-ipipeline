<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SubmitQuoteData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'who' => 'required|string',
            'years' => 'required|integer',
            'include_critical_illness' => 'required|boolean',
            'increasing_benefit' => 'required',
            'quote_based_on' => 'required',
            'benefit_amount' => 'required_if:quote_based_on,benefit',
            'premium_amount' => 'required_if:quote_based_on,premium',
            'client_one_gender' => 'required',
            'client_one_dob' => 'required',
            'client_one_smoker' => 'required',
            'client_one_title' => 'required',
            'client_one_first_name' => 'required',
            'client_one_last_name' => 'required',
            'client_one_email' => 'required|email',
            'client_two_gender' => 'required_if:who,joint',
            'client_two_dob' => 'required_if:who,joint',
            'client_two_smoker' => 'required_if:who,joint',
            'client_two_title' => 'required_if:who,joint',
            'client_two_first_name' => 'required_if:who,joint',
            'client_two_last_name' => 'required_if:who,joint',
            'client_two_email' => 'required_if:who,joint',
        ];
    }
}
